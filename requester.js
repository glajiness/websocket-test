var request = require("request");
var count = 0;

module.exports = function(req,res,next){
    function requestLoop(){
        setTimeout(function(){
            count++;
            if(count > req.body.num){
                next();
            }else{
                var options = {
                    url: req.body.url, 
                    headers:{ 
                        'request': count
                    }
                }
                console.log('make request ' + count);
                request(options, function (error, response, body) {
                    if(!error){
                        console.log('statusCode:', response && response.statusCode); 
                    }else{
                        console.log('statusCode:', response && response.statusCode); 
                        console.error('ERROR ', error);
                    }
                });
                requestLoop();
            }
        }, req.body.timeout)
    }
    requestLoop();
    console.log(req.body);
}