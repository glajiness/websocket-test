var async = require('async');
var _ = require('lodash');
var p = require('./puppeteer');
var info = require('./info')
var tasksQueue

function initQ(){
    tasksQueue = async.queue(async function (task, callback) {
        console.log('Performing task: ' + task.name);
        console.log('Waiting to be processed: ', tasksQueue.length());
        console.log('----------------------------------');
        await p()
    }, 1);
    console.log('queue initialized')
}

function pushQ(req,res,next){

    // When all is processed, drain is called
    tasksQueue.drain = function() {
        console.log('all items have been processed.');
    };
 
    function startTask(task){
        tasksQueue.push({name: task}, function(err) {
            //Done
            if (err) {
                console.log(err);
            }
            console.log('wtf part 2')
        });
    };

    startTask(info.task);
        
}

module.exports = {
    initQ,
    pushQ
}