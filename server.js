var express = require('express')
var ws = require('./ws')
var bodyParser = require('body-parser')
var requester = require('./requester')
var ws = require('./ws')
var q = require('./queue')

var app = express()
app.use(express.static('public'))

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
// parse application/json
app.use(bodyParser.json())

app.post('/', requester, function(req,res,next){
  res.send('works')
})

app.get('/', function (req, res) {
    ws()
    res.sendFile(__dirname + '/ws.html')
}, )


app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
  q.initQ()
})
