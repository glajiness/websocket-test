var WebSocketServer = require('ws').Server,
        wss = new WebSocketServer({port: 40510}),
        moment = require('moment'),
        info = require('./info.json'),
        q = require('./queue')

module.exports = function(req,res,next){
    wss.on('connection', function (ws) {
        ws.on('message', function (message) {
            console.log('received: %s', message)
        })
        setInterval(async () => {
            q.pushQ()
            ws.send(`
                ${ moment().format('MMMM Do YYYY, h:mm:ss a') }
                My name is Garrett... I just want wanted to say ${ info.title }
                ${ info.body }
            `);
        },info.interval)
    })

};